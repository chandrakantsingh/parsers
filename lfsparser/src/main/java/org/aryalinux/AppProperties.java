package org.aryalinux;

import java.util.Properties;

public class AppProperties {
	private static Properties properties = new Properties();

	static {
		try {
			properties.load(AppProperties.class.getClassLoader().getResourceAsStream("application.properties"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String getProperty(String name) {
		return properties.getProperty(name);
	}
}
