package org.aryalinux.lfsparser;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Globals {
	public static String bookDirectory;
	public static String outputDirectory;
	public static List<String> wgetList = new ArrayList<String>();
	public static int fileIndex;
	public static String scriptTemplatePath;
	public static String grubScriptTemplatePath;
	public static String kernelScriptTemplatePath;
	public static String logLineCountFile;

	public static void initBookDirectory(String bookDir) throws Exception {
		bookDirectory = bookDir;
		FileInputStream fin = new FileInputStream(bookDir + File.separator
				+ "wget-list");
		byte[] data = new byte[fin.available()];
		fin.read(data);
		fin.close();
		String str = new String(data);
		StringTokenizer stringTokenizer = new StringTokenizer(str, "\n");
		while (stringTokenizer.hasMoreTokens()) {
			String line = stringTokenizer.nextToken();
			if (!line.endsWith(".patch")) {
				wgetList.add(line.substring(line.lastIndexOf('/') + 1));
			}
		}
	}
}
