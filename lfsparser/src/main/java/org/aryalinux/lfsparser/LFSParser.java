package org.aryalinux.lfsparser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Map.Entry;

import org.aryalinux.AppProperties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LFSParser {
	public static void main(String[] args) throws Exception {
		Globals.initBookDirectory(AppProperties.getProperty("book.dir"));
		Globals.outputDirectory = AppProperties.getProperty("output.dir");
		Globals.scriptTemplatePath = AppProperties
				.getProperty("script.template.path");
		Globals.grubScriptTemplatePath = AppProperties
				.getProperty("grub.script.template.path");
		Globals.kernelScriptTemplatePath = AppProperties
				.getProperty("kernel.script.template.path");
		Globals.kernelScriptTemplatePath = AppProperties
				.getProperty("kernel.script.template.path");
		Globals.logLineCountFile = AppProperties
				.getProperty("loglinecount.file");
		Document document = Jsoup.parse(FileUtils.readRaw(Globals.bookDirectory
				+ File.separator + "index.html"));
		Elements elements = document.select("li.sect1 a");
		for (Element element : elements) {
			PageParser parser = new PageParser(Globals.bookDirectory
					+ File.separator + element.attr("href"));
			parser.parse();
			Page page = parser.getPage();
			if (page.getPagePath().contains("chapter05")
					|| page.getPagePath().contains("chapter06")) {
				Utils.incrementIndex();
				FileUtils.writeScript(page);
			}
			Utils.generateSQL(page);
			if (element.attr("href").contains("kernel")) {
				Map<String, Boolean> features = KernelPageParser
						.getConfigFeatures(Globals.bookDirectory
								+ File.separator + element.attr("href"));
				String fourdotsh = FileUtils
						.readRaw(Globals.kernelScriptTemplatePath);
				String script = "";
				for (Entry<String, Boolean> entry : features.entrySet()) {
					if (entry.getValue()) {
						script += "sed -i \"s@# " + entry.getKey()
								+ " is not set@" + entry.getKey()
								+ "=y@g\" .config\n";
					} else {
						script += "sed -i \"s@" + entry.getKey() + "=y@# "
								+ entry.getKey() + " is not set@g\" .config\n";
					}
				}
				fourdotsh = fourdotsh.replace("#CONFIG_LFS", "\n" + script);
				FileOutputStream fout = new FileOutputStream(
						Globals.outputDirectory + File.separator + "kernel.sh");
				fout.write(fourdotsh.getBytes());
				fout.close();
				File file = new File(Globals.outputDirectory + File.separator
						+ "kernel.sh");
				file.setExecutable(true);
			}
		}
		try {
			FileInputStream fin = new FileInputStream(
					AppProperties.getProperty("book.dir") + File.separator
							+ "wget-list");
			byte[] data = new byte[fin.available()];
			fin.read(data);
			fin.close();
			FileOutputStream fout = new FileOutputStream(
					AppProperties.getProperty("output.dir") + File.separator
							+ "wget-list");
			fout.write(data);
			fout.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
