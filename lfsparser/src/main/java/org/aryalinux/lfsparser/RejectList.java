package org.aryalinux.lfsparser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RejectList {
	public static List<String> commands;
	public static HashMap<String, String> replaceableParts;
	public static List<String> pages;
	public static List<String> commandPages;

	static {
		commands = new ArrayList<String>();
		commands.add("awk '/# PASS:/{total+=$3} ; END{print total}' gmp-check-log");
		commands.add("../contrib/test_summary");
		commands.add("exec /bin/bash --login +h");
		commands.add("passwd root");
		commands.add("echo \"quit\" | ./bc/bc -l Test/checklib.b");
		commands.add("ABI");
		commands.add("tzselect");
		commands.add("bash tests/run.sh --srcdir=$PWD --builddir=$PWD");
		commands.add("vim -c ':options'");

		pages = new ArrayList<String>();
		pages.add("kernfs");

		commandPages = new ArrayList<String>();
		commandPages.add("adjusting");
		
		replaceableParts = new HashMap<String, String>();
		replaceableParts.put("<xxx>", "$TIMEZONE");
	}
}
