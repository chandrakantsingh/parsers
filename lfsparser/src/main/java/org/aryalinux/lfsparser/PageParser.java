package org.aryalinux.lfsparser;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PageParser {
	private String pagePath;
	private Page page;

	public PageParser(String pagePath) {
		this.pagePath = pagePath;
	}

	public void parse() throws Exception {
		page = new Page();
		String html = FileUtils.readConverted(pagePath);
		Document document = Jsoup.parse(html);
		String title = document.select("title").html().trim();
		title = title.replace("&nbsp;", " ");
		page.setTitle(Utils.replaceEntities(title).trim());
		String index = title.split(" ")[0];
		String heading = title.substring(title.indexOf(" "));
		String tarball = Utils.findBestMatch(heading, Globals.wgetList, false);
		ArrayList<String> commands = new ArrayList<String>();
		Elements cmds = document.select("kbd.command");
		if (pagePath.contains("linux-headers")) {
			//commands.add("patch -Np1 -i ../linux-efivar.patch\n");
		}
		for (Element element : cmds) {
			String command = Utils.replaceEntities(element.html());
			boolean add = true;
			for (String str : RejectList.commands) {
				if (command.contains(str)) {
					add = false;
					System.out.println(command);
				}
			}
			if (command.contains("make") && (command.contains("test") || command.contains("check"))) {
				add = false;
			}
			if (command.contains("<xxx>")) {
				command = command.replace("<xxx>", "$TIMEZONE");
			}
			if (command.contains("<paper_size>")) {
				command = command.replace("<paper_size>", "PAPER_SIZE");
			}
			if (add == false) {
				continue;
			}
			commands.add(command);
		}
		if (title.toLowerCase().contains("libstdc++")) {
			tarball = Utils.findBestMatch("gcc", Globals.wgetList, false);
		}
		if (title.contains("XML::Parser")) {
			tarball = Utils.findBestMatch("XML-Parser", Globals.wgetList, false);
		}
		if (title.toLowerCase().contains("tcl")) {
			tarball = Utils.findBestMatch("tcl-core", Globals.wgetList, true);
		}
		if (title.toLowerCase().contains("man-db")) {
			tarball = Utils.findBestMatch("man-db", Globals.wgetList, true);
		}
		if (title.toLowerCase().contains("man-pages")) {
			tarball = Utils.findBestMatch("man-pages", Globals.wgetList, true);
		}
		if (title.toLowerCase().contains("util-linux")) {
			tarball = Utils.findBestMatch("util-linux", Globals.wgetList, true);
		}
		if (title.toLowerCase().contains("d-bus")) {
			tarball = Utils.findBestMatch("dbus", Globals.wgetList, false);
		}
		if (title.toLowerCase().contains("tcl-8")) {
			tarball = Utils.findBestMatch("tcl8", Globals.wgetList, false);
		}
		if (title.toLowerCase().contains("systemd-2")) {
			tarball = Utils.findBestMatch("systemd-2", Globals.wgetList, false);
		}
		if (title.toLowerCase().contains("libelf")) {
			tarball = Utils.findBestMatch("elf", Globals.wgetList, false);
		}
		page.setChapter(index);
		page.setPageName(pagePath.substring(pagePath.lastIndexOf('/') + 1).replace(".html", ""));
		page.setScriptName(Utils.index() + "-" + page.getPageName() + ".sh");
		page.setPagePath(pagePath);
		page.setTarball(tarball);
		page.setCommands(commands);
	}

	public Page getPage() {
		return page;
	}
}
