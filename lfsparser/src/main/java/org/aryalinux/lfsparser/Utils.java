package org.aryalinux.lfsparser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.aryalinux.AppProperties;

public class Utils {

	public static String findBestMatch(String title, List<String> wgetList,
			boolean includeDash) {
		List<String> words = new ArrayList<String>();
		StringTokenizer st = null;
		if (!includeDash) {
			st = new StringTokenizer(title.toLowerCase(), "- ");
		} else {
			st = new StringTokenizer(title.toLowerCase(), " ");
		}
		while (st.hasMoreTokens()) {
			String s = st.nextToken();
			words.add(s.toLowerCase());
		}
		HashMap<String, Integer> counts = new HashMap<String, Integer>();
		for (String item : wgetList) {
			String str = item.toLowerCase();
			for (String word : words) {
				String str1 = word.toLowerCase();
				if (str.startsWith(str1)) {
					if (!counts.keySet().contains(item)) {
						counts.put(item, 0);
					} else {
						counts.put(item, counts.get(item) + 1);
					}
				}
			}
		}
		String highestCountFor = null;
		int highestCount = 0;
		for (Entry<String, Integer> entry : counts.entrySet()) {
			if (entry.getValue() >= highestCount) {
				highestCount = entry.getValue();
				highestCountFor = entry.getKey();
			}
		}
		return highestCountFor;
	}

	public static String replaceEntities(String str) {
		String strResult = str.replace("&nbsp;", " ");
		strResult = strResult.replace("&lt;", "<");
		strResult = strResult.replace("&gt;", ">");
		strResult = strResult.replace("&amp;", "&");
		strResult = strResult.replace("&quot;", "\"");
		strResult = strResult.replace("br3ak", "\n");
		strResult = strResult.replace("<code class=\"literal\">", "");
		strResult = strResult.replace("</code>", "");
		String pattern = "(?i)(<em.*?><code>)(.+?)(</em>)";
		strResult = strResult.replaceAll(pattern, "$2");
		return strResult;
	}

	public static String index() {
		if (Globals.fileIndex < 10) {
			return "00" + Globals.fileIndex;
		} else if (Globals.fileIndex >= 10 && Globals.fileIndex < 100) {
			return "0" + Globals.fileIndex;
		} else {
			return "" + Globals.fileIndex;
		}
	}

	public static void incrementIndex() {
		Globals.fileIndex++;
	}

	public static void replaceInFile(String filePath, String find,
			String replace) throws Exception {
		FileInputStream fileInputStream = new FileInputStream(filePath);
		byte[] data = new byte[fileInputStream.available()];
		fileInputStream.read(data);
		fileInputStream.close();
		String str = new String(data);
		str.replace(find, replace);
		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		fileOutputStream.write(str.getBytes());
		fileOutputStream.close();
	}

	public static void generateSQL(Page page) throws Exception {
		if (page.getPagePath().contains("chapter06")) {
			String name = page.getScriptName().replace(".sh", "");
			name = name.substring(name.indexOf('-') + 1);
			FileUtils.append(AppProperties.getProperty("db.script"),
					"insert into packages (name, isPackage, isInstallable, packageId) values ('"
							+ name + "', '1', '0', '1');\n");
		}
	}
}