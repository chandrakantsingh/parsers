package org.aryalinux.lfsparser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class FileUtils {
	private static Map<String, Integer> logLines = new HashMap<String, Integer>();

	static {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(Globals.logLineCountFile)));
			String line = reader.readLine();
			while (line != null) {
				logLines.put(line.split(" ")[0],
						new Integer(line.split(" ")[1]));
				line = reader.readLine();
			}
			System.out.println(logLines);
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String readRaw(String pagePath) throws Exception {
		FileInputStream fileInputStream = new FileInputStream(pagePath);
		byte[] data = new byte[fileInputStream.available()];
		fileInputStream.read(data);
		fileInputStream.close();
		return new String(data).replace("=\n", "=");
	}

	public static String readConverted(String pagePath) throws Exception {
		FileInputStream fileInputStream = new FileInputStream(pagePath);
		byte[] data = new byte[fileInputStream.available()];
		fileInputStream.read(data);
		fileInputStream.close();
		return new String(data).replace("=\n", "=").replace("\n", "br3ak");
	}

	public static void writeScript(Page page) throws Exception {
		for (String str : RejectList.pages) {
			if (page.getScriptName().contains(str)) {
				return;
			}
		}
		File dir = null;
		if (page.getPagePath().contains("chapter05")) {
			dir = new File(Globals.outputDirectory + File.separator
					+ "toolchain");
		} else if (page.getPagePath().contains("chapter06")) {
			dir = new File(Globals.outputDirectory + File.separator
					+ "final-system");
		}
		if (dir != null) {
			dir.mkdirs();
		} else {
			return;
		}
		if (page.getTarball() == null || page.getCommands().size() == 0) {
			boolean shouldWrite = false;
			for (String s : RejectList.commandPages) {
				if (page.getScriptName().contains(s)) {
					shouldWrite = true;
				}
			}
			if (!shouldWrite) {
				return;
			}
		}
		File file = new File(dir, page.getScriptName());
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		InputStream scriptStream = null;
		if (page.getScriptName().contains("grub")) {
			scriptStream = new FileInputStream(Globals.grubScriptTemplatePath);
		} else {
			scriptStream = new FileInputStream(Globals.scriptTemplatePath);
		}
		byte[] data = new byte[scriptStream.available()];
		scriptStream.read(data);
		scriptStream.close();
		String script = new String(data);
		script = script.replace("stepname", page.getScriptName());
		if (page.getTarball() != null) {
			script = script.replace("tarball", page.getTarball());
		} else {
			script = script.replace("tarball", "");
		}
		List<String> commandLines = new ArrayList<String>();
		for (String cmd : page.getCommands()) {
			StringTokenizer st = new StringTokenizer(cmd, "\n");
			while (st.hasMoreTokens()) {
				commandLines.add(st.nextToken());
			}
		}
		String commands = "";
		for (String cmd : commandLines) {
			boolean add = true;
			for (String str : RejectList.commands) {
				if (cmd.contains(str)) {
					add = false;
					System.out.println(cmd);
				}
			}
			if (add == false) {
				continue;
			}
			commands += cmd + "\n";
		}
		script = script.replace("commands", commands);
		if (page.getPageName().contains("groff")) {
			script = script.replace("export MAKEFLAGS=\"-j `nproc`\"",
					"export MAKEFLAGS=\"-j 1\"");
		}
		script = script.replace("LOG_LENGTH",
				"" + logLines.get(page.getScriptName()));
		fileOutputStream.write(script.getBytes());
		fileOutputStream.close();
		file.setExecutable(true);
	}

	public static void append(String filename, String line) throws Exception {
		try {
			RandomAccessFile raf = new RandomAccessFile(filename, "rw");
			raf.seek(raf.length());
			raf.write(line.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
