package org.aryalinux.lfsparser;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class KernelPageParser {

	public static Map<String, Boolean> getConfigFeatures(String pagePath) throws Exception {
		Map<String, Boolean> features = new HashMap<String, Boolean>();
		if (pagePath.contains("kernel")) {
			Document document = Jsoup.parse(FileUtils.readConverted(pagePath));
			String configContents = document.select("pre.screen").html();
			configContents = configContents.replace("br3ak", "\n");
			StringTokenizer stringTokenizer = new StringTokenizer(configContents, "\n");
			while (stringTokenizer.hasMoreTokens()) {
				String line = stringTokenizer.nextToken();
				if (line.contains("[ ]") || line.contains("[*]")) {
					String str = line.substring(line.lastIndexOf("["));
					str = str.replace("[", "");
					str = str.replace("]", "");
					if (line.contains("[ ]")) {
						features.put(str, false);
					} else {
						features.put(str, true);
					}
				}
			}
		}
		return features;
	}
}
