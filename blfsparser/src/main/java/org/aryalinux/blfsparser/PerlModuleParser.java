package org.aryalinux.blfsparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aryalinux.blfsparser.util.CommandUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PerlModuleParser implements Parser {

	@Override
	public List<Package> parse(String fileName, Document document) {
		List<Package> result = new ArrayList<Package>();
		Elements elements = document.select("div.itemizedlist a.xref");
		for (Element element : elements) {
			Package p = new Package();
			// Skip perl-alternatives
			if (element.attr("href").endsWith("perl-alternatives")) {
				continue;
			}
			String perlModulePackageName = element.attr("href").split("#")[1];
			String name = element.attr("title");
			Elements containerDivs = document.select("div.sect2");
			Element containerDiv = containerFor(containerDivs, perlModulePackageName);
			String downloadUrl = containerDiv.select("a.ulink").attr("href");
			Elements requiredDeps = containerDiv.select("p.required a");
			Elements recommendedDeps = containerDiv.select("p.recommended a");
			Elements optionalDeps = containerDiv.select("p.optional a");
			List<String> downloadUrls = Arrays.asList(downloadUrl);
			String[] urlParts = downloadUrl.split("/");
			String tarball = urlParts[urlParts.length - 1];
			String[] tarballParts = tarball.split("-");
			String version = tarballParts[tarballParts.length - 1].replace(".tar.gz", "").replace(".tar.bz2", "")
					.replace(".tar.xz", "").replace(".tgz", "").replace(".lzma", "").replace(".tar", "");
			for (Element requiredDep : requiredDeps) {
				if (null != packageNameFromHref(requiredDep.attr("href")))
					p.addRequiredDependency(packageNameFromHref(requiredDep.attr("href")));
			}
			for (Element recommendedDep : recommendedDeps) {
				if (null != packageNameFromHref(recommendedDep.attr("href")))
					p.addRecommendedDependency(packageNameFromHref(recommendedDep.attr("href")));
			}
			for (Element optionalDep : optionalDeps) {
				if (null != packageNameFromHref(optionalDep.attr("href")))
					p.addOptionalDependency(packageNameFromHref(optionalDep.attr("href")));
			}
			Elements pres = containerDiv.select("pre");
			List<String> commands = new ArrayList<String>();
			for (Element pre : pres) {
				if (pre.attr("class").equals("userinput")) {
					commands.add("user: " + pre.select("kbd.command").first().html());
				} else if (pre.attr("class").equals("root")) {
					commands.add("root: " + pre.select("kbd.command").first().html());
				}
			}
			commands = postProcessCommands(commands);
			p.setName(name.toLowerCase());
			p.setPackageName(perlModulePackageName.toLowerCase());
			p.setVersion(version);
			p.setCommands(commands);
			p.setDownloadUrls(downloadUrls);
			if (p.getDownloadUrls().size() > 0) {
				p.setUrl(p.getDownloadUrls().get(0));
			}
			p.setCommands(CommandUtil.processCommands(p.getCommands()));
			result.add(p);
		}
		return result;
	}

	private Element containerFor(Elements containerDivs, String perlModulePackageName) {
		for (Element containerDiv : containerDivs) {
			if (containerDiv.select("a#" + perlModulePackageName).size() != 0) {
				return containerDiv;
			}
		}
		return null;
	}

	private String packageNameFromHref(String href) {
		if (href.endsWith("html")) {
			String[] parts = href.split("/");
			return parts[parts.length - 1].replace(".html", "");
		} else if (href.contains("#perl")) {
			return href.split("#")[1];
		} else {
			return null;
		}
	}

	private List<String> postProcessCommands(List<String> commands) {
		List<String> result = new ArrayList<String>();
		for (String command : commands) {
			result.add(command.replace("&amp;", "&"));
		}
		return result;
	}
}
