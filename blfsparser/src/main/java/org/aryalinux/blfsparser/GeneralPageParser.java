package org.aryalinux.blfsparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aryalinux.blfsparser.util.CommandUtil;
import org.aryalinux.blfsparser.util.UrlUtil;
import org.aryalinux.blfsparser.util.VersionUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GeneralPageParser implements Parser {

	@Override
	public List<Package> parse(String fileName, Document document) {
		Package p = new Package();
		String[] parts = fileName.split("/");
		String name = parts[parts.length - 1].replace(".html", "");
		p.setPackageName(name.toLowerCase());
		p.setName(name.toLowerCase());
		Elements downloadLinks = document.select("p a.ulink");
		p.setDownloadUrls(new ArrayList<String>());
		for (Element downloadLink : downloadLinks) {
			String url = downloadLink.attr("href");
			if (UrlUtil.isDownloadUrl(url)) {
				p.getDownloadUrls().add(url);
			}
		}
		Elements requiredLinks = document.select("p.required a");
		Elements recommendedLinks = document.select("p.recommended a");
		Elements optionalLinks = document.select("p.optional a");
		p.setRequiredDependencies(new ArrayList<String>());
		p.setRecommendedDependencies(new ArrayList<String>());
		p.setOptionalDependencies(new ArrayList<String>());

		for (Element requiredLink : requiredLinks) {
			String packageName = UrlUtil.getPackageNameFromUrl(requiredLink.attr("href"));
			if (null != packageName) {
				p.addRequiredDependency(packageName);
			}
		}
		for (Element recommendedLink : recommendedLinks) {
			String packageName = UrlUtil.getPackageNameFromUrl(recommendedLink.attr("href"));
			if (null != packageName) {
				p.addRecommendedDependency(packageName);
			}
		}
		for (Element optionalLink : optionalLinks) {
			String packageName = UrlUtil.getPackageNameFromUrl(optionalLink.attr("href"));
			if (null != packageName) {
				p.addOptionalDependency(packageName);
			}
		}

		p.setCommands(new ArrayList<String>());

		Elements commandElements = document.select("pre");
		for (Element commandElement : commandElements) {
			if (commandElement.select("kbd.command").size() > 0) {
				if (commandElement.attr("class").equals("userinput")) {
					p.getCommands()
							.add("user: " + CommandUtil.process(commandElement.select("kbd.command").first().html()));
				} else if (commandElement.attr("class").equals("root")) {
					p.getCommands()
							.add("root: " + CommandUtil.process(commandElement.select("kbd.command").first().html()));
				}
			}
		}

		if (p.getDownloadUrls().size() > 0) {
			p.setVersion(VersionUtil.guessVersion(p.getDownloadUrls().get(0)));
		}
		if (p.getDownloadUrls().size() > 0) {
			p.setUrl(p.getDownloadUrls().get(0));
		}
		p.setCommands(CommandUtil.processCommands(p.getCommands()));
		return Arrays.asList(p);
	}
}
