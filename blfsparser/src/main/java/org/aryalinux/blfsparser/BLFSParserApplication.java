package org.aryalinux.blfsparser;

import org.aryalinux.blfsparser.generator.ScriptGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import freemarker.template.TemplateExceptionHandler;

@Configuration
@SpringBootApplication
public class BLFSParserApplication {

	@Bean
	public freemarker.template.Configuration freemarkerConfiguration() {
		freemarker.template.Configuration cfg = new freemarker.template.Configuration(
				freemarker.template.Configuration.VERSION_2_3_27);
		cfg.setClassForTemplateLoading(ScriptGenerator.class, "/templates/");
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		cfg.setWrapUncheckedExceptions(true);
		return cfg;
	}

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(BLFSParserApplication.class, args);
		context.getBean(BLFSParser.class).parse(args);
	}
}
