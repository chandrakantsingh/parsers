package org.aryalinux.blfsparser;

import java.util.ArrayList;
import java.util.List;

public class Package {
	private String name;
	private String version;
	private List<String> downloadUrls;
	private String packageName;
	private List<String> requiredDependencies;
	private List<String> recommendedDependencies;
	private List<String> optionalDependencies;
	private List<String> commands;
	private String url;

	public Package() {
		requiredDependencies = new ArrayList<String>();
		recommendedDependencies = new ArrayList<String>();
		optionalDependencies = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<String> getDownloadUrls() {
		return downloadUrls;
	}

	public void setDownloadUrls(List<String> downloadUrls) {
		this.downloadUrls = downloadUrls;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public List<String> getRequiredDependencies() {
		return requiredDependencies;
	}

	public void setRequiredDependencies(List<String> requiredDependencies) {
		this.requiredDependencies = requiredDependencies;
	}

	public List<String> getRecommendedDependencies() {
		return recommendedDependencies;
	}

	public void setRecommendedDependencies(List<String> recommendedDependencies) {
		this.recommendedDependencies = recommendedDependencies;
	}

	public List<String> getOptionalDependencies() {
		return optionalDependencies;
	}

	public void setOptionalDependencies(List<String> optionalDependencies) {
		this.optionalDependencies = optionalDependencies;
	}

	public List<String> getCommands() {
		return commands;
	}

	public void setCommands(List<String> commands) {
		this.commands = commands;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void addRequiredDependency(String dependency) {
		dependency = dependency.replace("perl-modules#", "");
		dependency = dependency.replace("python-modules#", "");
		dependency = dependency.replace("x7driver#", "");
		requiredDependencies.add(dependency);
	}

	public void addRecommendedDependency(String dependency) {
		dependency = dependency.replace("perl-modules#", "");
		dependency = dependency.replace("python-modules#", "");
		dependency = dependency.replace("x7driver#", "");
		recommendedDependencies.add(dependency);
	}

	public void addOptionalDependency(String dependency) {
		dependency = dependency.replace("perl-modules#", "");
		dependency = dependency.replace("python-modules#", "");
		dependency = dependency.replace("x7driver#", "");
		optionalDependencies.add(dependency);
	}

}
