package org.aryalinux.blfsparser;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.aryalinux.blfsparser.generator.ScriptGenerator;
import org.aryalinux.blfsparser.generator.ScriptGeneratorFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BLFSParser {
	@Autowired
	private ScriptGeneratorFactory scriptGeneratorFactory;
	@Value("${dir.base}")
	private String baseDir;
	@Value("${dir.output}")
	private String outDir;

	public void parse(String[] args) {
		Document doc;
		try {
			List<Package> allPackages = new ArrayList<Package>();
			doc = Jsoup.parse(new File(baseDir + "index.html"), "utf8");
			Elements links = doc.select("li.sect1 a");
			for (Element link : links) {
				String href = link.attr("href");
				href = baseDir + href;
				Document document = Jsoup.parse(new File(href), "utf8");
				Parser parser = null;
				List<Package> packages = null;
				if (href.contains("perl-modules.html") || href.contains("perl-deps.html")
						|| href.contains("x7driver.html") || href.contains("python-modules.html")) {
					parser = new PerlModuleParser();
					packages = parser.parse(href, document);
				} else {
					parser = new GeneralPageParser();
					packages = parser.parse(href, document);
				}
				allPackages.addAll(packages);
			}
			if (!new File(outDir).exists()) {
				new File(outDir).mkdirs();
			}
			for (Package p : allPackages) {
				String scriptOutput = scriptGeneratorFactory.getGenerator(ScriptGenerator.DEFAULT).generate(p);
				FileOutputStream fout = new FileOutputStream(outDir + p.getPackageName() + ".sh");
				fout.write(scriptOutput.getBytes());
				fout.close();
				File f = new File(outDir + p.getPackageName() + ".sh");
				f.setExecutable(true);
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
