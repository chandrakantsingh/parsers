package org.aryalinux.blfsparser.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ScriptGeneratorFactory {
	@Autowired
	private ApplicationContext context;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public final ScriptGenerator getGenerator(Class clazz) {
		try {
			return (ScriptGenerator) context.getBean(clazz);
		} catch (Exception ex) {
			throw new RuntimeException("Failed to create a generator instance for : " + clazz);
		}
	}
}
