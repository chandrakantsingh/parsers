package org.aryalinux.blfsparser.generator;

import org.aryalinux.blfsparser.Package;
import org.aryalinux.blfsparser.generator.impl.DefaultScriptGenerator;

public interface ScriptGenerator {
	@SuppressWarnings("rawtypes")
	public static final Class DEFAULT = DefaultScriptGenerator.class;

	String generate(Package p);
}
