package org.aryalinux.blfsparser.generator.impl;

import java.io.StringWriter;

import org.aryalinux.blfsparser.Package;
import org.aryalinux.blfsparser.generator.ScriptGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Component
public class DefaultScriptGenerator implements ScriptGenerator {
	@Autowired
	private Configuration configuration;

	@Override
	public final String generate(Package p) {
		try {

			Template template = configuration.getTemplate("script.ftl");
			StringWriter stringWriter = new StringWriter();
			template.process(p, stringWriter);
			return stringWriter.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
