package org.aryalinux.blfsparser.util;

public class VersionUtil {
	public static final String[] EXTENSIONS = { ".tar.gz", "tar.bz2", ".tar.xz", ".zip", ".tgz", ".lzma", ".tar" };

	public static final String guessVersion(String url) {
		try {
			String[] parts = url.split("/");
			String tarball = parts[parts.length - 1];
			for (String extension : EXTENSIONS) {
				if (tarball.endsWith(extension)) {
					tarball = tarball.replace(extension, "");
				}
			}
			boolean found = false;
			int index = tarball.length() - 1;
			for (int i = index; i > 0; i--) {
				if (tarball.charAt(i) == '-' && Character.isAlphabetic(tarball.charAt(i - 1))) {
					found = true;
					index = i;
					break;
				}
			}
			if (found) {
				return tarball.substring(index + 1);
			}
			else {
				return null;
			}
		} catch (Exception ex) {
			return null;
		}
	}
}
