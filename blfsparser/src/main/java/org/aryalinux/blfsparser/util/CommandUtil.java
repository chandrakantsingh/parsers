package org.aryalinux.blfsparser.util;

import java.util.ArrayList;
import java.util.List;

public class CommandUtil {
	public static final String process(String command) {
		String result = command.replace("&amp;", "&").replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "\"");
		return result;
	}

	public static final List<String> processCommands(List<String> commands) {
		List<String> processedCommands = new ArrayList<String>();
		for (String command : commands) {
			if (command.startsWith("root: ")) {
				command = command.replace("root: ", "");
				String str = "\nsudo rm /tmp/rootscript.sh\n";
				str = str + "cat > /tmp/rootscript.sh <<\"EOF\"\n";
				str = str + command + "\n";
				str = str + "EOF\n";
				str = str + "chmod a+x /tmp/rootscript.sh\n";
				str = str + "sudo /tmp/rootscript.sh\n";
				str = str + "sudo rm /tmp/rootscript.sh\n";
				processedCommands.add(str);
			} else {
				processedCommands.add(command.replace("user: ", "").trim());
			}
		}
		return processedCommands;
	}
}
