package org.aryalinux.blfsparser.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtil {
	public static String objectToString(Object ref) {
		try {
			return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(ref);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
}
