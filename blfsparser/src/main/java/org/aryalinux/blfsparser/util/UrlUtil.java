package org.aryalinux.blfsparser.util;

public class UrlUtil {
	public static final boolean isDownloadUrl(String url) {
		return url.endsWith(".tar.gz") || url.endsWith(".tar.bz2") || url.endsWith(".tgz") || url.endsWith(".tar")
				|| url.endsWith(".lzma") || url.endsWith(".tgz") || url.endsWith(".tar.xz") || url.endsWith(".zip")
				|| url.endsWith(".patch");
	}

	public static final String getPackageNameFromUrl(String url) {
		if (url.endsWith(".html") || url.contains(".html#")) {
			String[] parts = url.split("/");
			return parts[parts.length - 1].replace(".html", "");
		} else {
			return null;
		}
	}
}
