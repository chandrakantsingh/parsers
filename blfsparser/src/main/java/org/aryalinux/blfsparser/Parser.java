package org.aryalinux.blfsparser;

import java.util.List;

import org.jsoup.nodes.Document;

public interface Parser {
	public List<Package> parse(String fileName, Document document);
}
